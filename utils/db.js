const mongoose =require("mongoose");

const hostname="localhost";
const dbName="test";
const port="27017";
const CONN_DB_STR = `mongodb://${hostname}:${port}/${dbName}`;
//连接mongo数据库
let conn=(callback)=>{
    mongoose.connect(CONN_DB_STR,{
        useNewUrlParser:true,
        useUnifiedTopology:true,
    });
    const db=mongoose.connection;
    db.on('error',(err)=>{
        console.log('数据库连接失败');
        callback(err,null)  
    })
    db.once('open',()=>{
        console.log('数据库连接成功');
        callback(null,mongoose)  
    });
    db.on('disconnected',function(){
        console.log('数据库连接断开')
    })
}
module.exports=conn;