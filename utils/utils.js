var fs=require("fs");
var http=require('http');
var https=require('https');
var cheerio=require('cheerio');
let key = "cty@2020"
let crypto;
try {
    crypto = require('crypto');//密码 node模块
} catch (err) {
    console.log('不支持 crypto')
}
/**
 * 加密
 * @param {明文} data 
 */
exports.aesEncrypt=function (data) {
    const cipher = crypto.createCipher('aes192', key);
    var crypted = cipher.update(data, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}
/**
 * 解密
 * @param {密文} encrypted 
 */
exports.aesDecrypt=function (encrypted) {
    const decipher = crypto.createDecipher('aes192', key);
    var decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

/**
 * 错误处理
 * @param {错误} err 
 * @param {返回} res 
 * @param {数据库} db 
 */
exports.setError=function (err,res,db){
    if(err){
        res.json({
            code:1,
            msg:"数据库错误..."
        })
        db.disconnect()
    }
}
/**
 * 读取目录-获取文件
 */
exports.getFiles=function (fileUrl,callback){
    fs.readdir(fileUrl,(err,files)=>{
        if(err) throw err;
        callback(files)
    })
}

/**
 * 网页小爬虫
 */
exports.getHTMLContent=function(url,callback){
    let HTTP
    let regHTTPS=/https/ig;
    if(regHTTPS.test(url)){
        HTTP=https
    }else{
        HTTP=http
    }
    function printImgURL(data,callback){
      
        var $=cheerio.load(data);
        var len=$('ducoment').find('script');
        callback(data)
    }
    HTTP.get(url,(res)=>{
      
        const {statusCode}=res;
        console.log(`响应headers：${JSON.stringify(res.headers)}`);
        let error
        if(statusCode!==200){
            error=new Error(`请求失败\n状态码：${statusCode}`)
        }
        if(error){
            console.error(error.message);
            res.resume();
            return;
        }
        res.setEncoding('utf8');
        let rawData="";
        res.on('data',(chunk)=>{
            rawData+=chunk
        });
        res.on('end',()=>{
            try{
                // const parsedData=JSON.parse(rawData);
                // console.log("爬取内容：",rawData);
               
                printImgURL(rawData,callback)
            }catch(e){
                console.log(e.message)
            }
        })
    }).on('error',(e)=>{
        console.log(`出现错误：${e.message}`)
    })
    
}
/**
 * 是否登录
 * @param {请求参数} req 
 * @param {返回数据} res 
 * @param {前台token} token 
 */
exports.isLogin=function (req,res,callback){
    var cookie=req.headers.cookie;
    if(cookie){
        var arr=cookie.split(';');
        let tokenArr=[];
        let token=""
        var reg=/token/g;
        for(let i=0;i<arr.length;i++){
            let item=arr[i];
            let flag=reg.test(item);
            if(flag){
                tokenArr=item.split('=');
                token=tokenArr[1]
              
            }
        }
        if(req.session.token!==token){
          res.json({code:401,msg:'无合法授权或者授权失效，请重新登录'})  
        }else{
            callback()
        }
    }else{
        res.json({code:401,msg:'无合法授权或者授权失效，请重新登录'})  
    }
   
}