var express=require('express');
var conn=require('../utils/db')
var {AccountModel}=require('../model/schema')
var {aesEncrypt,aesDecrypt,setError}=require('../utils/utils')
var {waterfall,series,parallel }=require('async')
var CONST =require('../const/const')
//waterfall 串行有关联 数组
//parallel  并行无关联 对象
//series  串行无关联 对象
var router=express();

/** 
 * 登录
*/
router.post('/',function(req,res){ //登录
    var params=req.body;
    conn((err,db)=>{
        setError(err,res,db);
        if(err) throw  err;
        waterfall([
            function(callback){
                AccountModel.find({'accounts':params.accounts},(err,result)=>{
                    if(result.length>0){
                        callback(err,result)
                    }else{
                        callback(err,null)
                    }
                   
                })
            },
            function(result,callback){
                if(result){
                    if(result[0].password===aesDecrypt(params.password)){
                       
                        if(params.verificationCode=== req.session.verificationCode){
                            var token=aesEncrypt(JSON.stringify(result));
                            req.session.token=token;
                            res.cookie('token', token, {maxAge:CONST.OVERDUE_TIME, httpOnly: false}) // 该处是设置 cookie 与 httpOnly 
                            callback(err,{code:1,msg:"登陆成功",data:{token:token}});
                        }else{
                            callback(err,{code:0,msg:"验证码错误"});
                        }
                      
                    }else{
                        callback(err,{code:0,msg:"密码错误"});
                    }

                }else{
                  callback(err,{code:0,msg:"用户名不存在"});
                }
             
           
            }
        ],function(err,result){
            setError(err,res,db);
            res.json(result);
            db.disconnect() // 断开
        })
    })
});

/**
 * 生成验证码
 */

router.get("/verificationCode",function(req,res){  //产生验证码，返给前端
    let len=4;
    let codeStr=""
    for(let i=0;i<len;i++){
        let randomNum=Math.floor(Math.random()*10);
        codeStr+=randomNum
    }
    req.session.verificationCode=codeStr;
        res.json({
            code:1,
            result:{
                verificationCode:codeStr
            },
            msg:"验证码已生成"
        });
});


/**
 * 
 */
router.post("/register",function(req,res){
       // params=Object.assign({
                    //     createTime:new Date()
                    // },params)
                    // let ducoment=new AccountModel(params);
                    // ducoment.save(function(err,result){
                       
                    //     callback(err,{code:1,data:result,msg:"成功"})
                    // })
                    // AccountModel.find({'accounts':params.accounts,"password":params.password},(err,result)=>{
                    //     if(result.length>0){
                  
                    //     }else{
                          
                    //     }
                       
                    // })
})



module.exports=router; 