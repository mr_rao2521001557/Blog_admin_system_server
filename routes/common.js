var express=require('express');
var router=new express();
var {aesEncrypt,aesDecrypt,setError,getFiles,getHTMLContent}=require('../utils/utils')

router.get('/backgroundImage',function(req,res){
   
    getFiles('./public/images',(files)=>{
        let imgArr=[];
        for(let i=0;i<30;i++){
            let img=files[Math.floor(Math.random()*files.length)]
            imgArr.push(img)
        }
        var newImgArr=Array.from(new Set(imgArr))
        res.json({code:1,msg:"动态壁纸加载ok",data:newImgArr})
    })
})

router.get('/getBackgroundMusic',function(req,res){
   
    getFiles('./public/backgroundMusic',(files)=>{
        let musicArr=[];
        for(let i=0;i<20;i++){
            let music=files[Math.floor(Math.random()*files.length)]
            musicArr.push(music)
        }
        res.json({code:1,msg:"背景音乐加载ok",data:musicArr})
    })
})
router.get('/spider',function(req,res){
    getHTMLContent('https://image.baidu.com/search/index?tn=baiduimage&ct=201326592&lm=-1&cl=2&ie=gb18030&word=%B6%FE%B4%CE%D4%AA%B8%DF%C7%E5%B1%DA%D6%BD&fr=ala&ala=1&alatpl=adress&pos=0&hs=2&xthttps=111111',(result)=>{
        res.send(result)
    })
})
module.exports=router