var mongoose=require('mongoose');
var accountSchema=mongoose.Schema({
    accounts:String,//用户名
    password:String,//密码
    createTime:Date,//账户创建时间
})

var accountModel=mongoose.model('account',accountSchema) //账户表
exports.AccountModel=accountModel;