var createError = require('http-errors'); 
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session=require("express-session");
var app = express();
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');
var testRouter=require('./routes/test');
var loginRouter=require('./routes/login');//登录模块
var commonRouter=require('./routes/common');
var CONST =require('./const/const')
console.log('express项目入口文件')
// view engine setup  查看引擎设置
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  name:"sessionID",
  secret:"cty@2020",//设置签名密钥，内容可以任意
  cookie:{maxAge:CONST.OVERDUE_TIME},//设置cookit的过期时间
  resave:false,//强制保存，如果session没有被修改也要重新保存
  saveUninitialized:true,//如果原先没有sesion那么就社会，否则就不设置
}))
app.use(express.static(path.join(__dirname, 'public')));

// ====分割线=====
app.use(express.static('public'));//指定静态资源文件目录
app.use(express.static('files'));//指定静态资源文件目录
//=====分割线=====

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/admin', adminRouter);
app.use('/test',testRouter)
app.use('/login',loginRouter)
app.use('/common',commonRouter)
// 捕获404并转发到错误处理程序
app.use(function(req, res, next) {  
  next(createError(404));
});

// 错误处理程序
app.use(function(err, req, res, next) {
  // 设置局部变量，只在开发中提供错误
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // 呈现错误页
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;
